DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS account;
create table account(id int primary key auto_increment,firstName varchar(20),lastName varchar(20),userName varchar(20),
password varchar(20),
email varchar(30),phone varchar(15),startBalance float4,
accountNumber varchar(40),createDate datetime,modifyDate datetime);

DROP TABLE IF EXISTS transaction;
create table transaction(id int primary key auto_increment,reference int4,transaction_amount float4,account_id int,description varchar(50),
transaction_type varchar(50),startBalance float4,endBalance float4,transactionDate datetime,
FOREIGN KEY (`account_id`) REFERENCES `account` (`id`));