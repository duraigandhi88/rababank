package com.rabobank.util;

/**
 * 
 * @author gandhi.d
 *
 */
public class ResponseCode {
	
	public static final Integer SUCCESS=200;
	public static final Integer INTERNAL_SERVER_ERROR=500;
	public static final Integer BAD_REQUEST=400;
	public static final Integer FORBIDDEN=403;
	public static final Integer UNAUTHORIZED=401;
	public static final Integer CONFLICT=409;
	public static final Integer NO_DATA=503;
	

}
