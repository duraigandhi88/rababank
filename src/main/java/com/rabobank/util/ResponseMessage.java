package com.rabobank.util;

/**
 * 
 * @author gandhi.d
 *
 */

public class ResponseMessage {
	
	public static final String SUCCESS="Success";
	public static final String INTERNAL_SERVER_ERROR="Internal Server Error";
	public static final String BAD_REQUEST="Bad Reuest";
	public static final String FORBIDDEN="Forbidden";
	public static final String UNAUTHORIZED="Unauthorized";
	public static final String TOKEN_EXPIRED="Token Expired";
	public static final String INVALID_TOKEN="Token Invalid";
	public static final String CONFLICT="Duplicate Data";
	public static final String USERNAME_INVALID="Username is incorrect";
	public static final String PASSWORD_INVALID="Password is incorrect";
	public static final String TRANSACTION_INVALID="Transaction is incorrect";
	public static final String ACCOUNT_NUMBER_INVALID="Account number is incorrect";
	public static final String INITIAL_AMOUNT="initial balance";
	public static final String NO_DATA="Service unavailable";
	public static final String USERNAME_EXISTS="Username already exists";
	public static final String EMAIL_EXISTS="Email already exists";
	public static final String PHONE_EXISTS="Phone already exists";
	

}
