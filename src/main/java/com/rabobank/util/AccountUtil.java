package com.rabobank.util;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.iban4j.CountryCode;
import org.iban4j.Iban;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.rabobank.beans.Account;
import com.rabobank.beans.ResponseBean;
import com.rabobank.beans.Transaction;
import com.rabobank.repository.AccountRepository;
import com.rabobank.validate.Validate;

@Service
public class AccountUtil {
	private static final Logger LOG=LogManager.getLogger(AccountUtil.class);
	@Autowired
	private AccountRepository accountRepository;
	
	@Value("${referanceStartDigit}")
	private Integer referanceStartDigit;
	
	@Value("${referanceEndDigit}")
	private Integer referanceEndDigit;
	
	
	/**
	 * 
	 * @return IBAN Random account number
	 * @throws Exception
	 */
	public String getRandomIBANAccountNumber() throws Exception {
		String ibanAccountNumber=Iban.random(CountryCode.NL).toString();
		LOG.info("IBAN Generated");
		if(accountRepository.findByAccountNumber(ibanAccountNumber)!=null) {
			getRandomIBANAccountNumber();
		}
		return ibanAccountNumber;
	}
	
	
	/**
	 * 
	 * @param userName
	 * @param password
	 * @return validate the credentials
	 */
	public ResponseEntity<?> doLogin(String userName,String password) {
		ResponseBean responseBean=Validate.validateUserNameAndPassword(userName, password);
		Account accountTransaction=accountRepository.findByUserNameAndPassword(userName,password);
		if(responseBean!=null) {
			LOG.info("Login faild");
			return ResponseData.validatitionFails(responseBean.getStatusMessage());
		}
		else if(accountTransaction!=null) {
			LOG.info("Login success");
			return ResponseData.successData(accountTransaction);
		}
		LOG.info("Login unauthorized");
		return ResponseData.unAutherized(ResponseMessage.UNAUTHORIZED);	
	}
	
	
	
	
	
	/**
	 * 
	 * @return unique reference six digits number
	 */
	public Integer getRandomReferenceNumber() {
		Random randomNumber=new Random();
		return randomNumber.nextInt(referanceEndDigit-referanceStartDigit)+referanceStartDigit;
	}
	
	/**
	 * 
	 * @param withdrawAmount
	 * @param accountNumber
	 * @return the endbalance {exitingbalance-withdrawAmount}
	 */
	public Float doCalEndBalanceWithWithdraw(Transaction transaction,final Account account) {
		float endBalance=0f;
		if(account.getLiTransactions()!=null && account.getLiTransactions().size()>0) {
			LOG.info("endbalce calculated based on withdraw from last transaction");
			float beforeEndBalance=account.getLiTransactions().get(account.getLiTransactions().size()-1).getEndBalance();
			transaction.setStartBalance(beforeEndBalance);
			endBalance=beforeEndBalance-transaction.getTransactionAmount();
		}
		return endBalance;
	}
	
	
	/**
	 * 
	 * @param transferAmount
	 * @param accountNumber
	 * @return the endbalance {transferAmount+exitingbalance}
	 */
	public Float doCalEndBalanceWithTransfer(Transaction transaction,final Account account) {
		float endBalance=0f;
		if(account.getLiTransactions()!=null && account.getLiTransactions().size()>0) {
			LOG.info("endbalce calculated based on fund transer from last transaction");
			float beforeEndBalance=account.getLiTransactions().get(account.getLiTransactions().size()-1).getEndBalance();
			transaction.setStartBalance(beforeEndBalance);
			endBalance=beforeEndBalance+transaction.getTransactionAmount();
		}
		return endBalance;
	}
	
	
	
	
	
	
	

}
