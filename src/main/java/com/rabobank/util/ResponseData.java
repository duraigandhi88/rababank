package com.rabobank.util;



import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.rabobank.beans.Account;
import com.rabobank.beans.ResponseBean;

/**
 * 
 * @author gandhi.d
 *
 */
@RestController
public class ResponseData {

	public static ResponseEntity<?> internalError(String data)  {
		ResponseBean responseData = new ResponseBean();
		responseData.setStatusCode(ResponseCode.INTERNAL_SERVER_ERROR);
		responseData.setStatusMessage(data);
		return ResponseEntity.status(ResponseCode.SUCCESS).body(responseData);
	}
	
	public static ResponseEntity<?> unAutherized(Object data)  {
		ResponseBean responseData = new ResponseBean();
		responseData.setStatusCode(ResponseCode.UNAUTHORIZED);
		responseData.setStatusMessage(ResponseMessage.UNAUTHORIZED);
		return ResponseEntity.status(ResponseCode.SUCCESS).body(responseData);
	}

	public static ResponseEntity<?> success(String data)  {
		ResponseBean responseData = new ResponseBean();
		responseData.setStatusCode(ResponseCode.SUCCESS);
		responseData.setStatusMessage(data);
		return ResponseEntity.status(ResponseCode.SUCCESS).body(responseData);
	}
	
	public static ResponseEntity<?> successData(Account data)  {
		ResponseBean responseData = new ResponseBean();
		responseData.setStatusCode(ResponseCode.SUCCESS);
		responseData.setData(data);
		return ResponseEntity.status(ResponseCode.SUCCESS).body(responseData);
	}
	
	
	
	public static ResponseEntity<?> validatitionFails(String data)  {
		ResponseBean responseData = new ResponseBean();
		responseData.setStatusCode(ResponseCode.BAD_REQUEST);
		responseData.setStatusMessage(data);
		return ResponseEntity.status(ResponseCode.SUCCESS).body(responseData);
	}

	
		
	public static ResponseEntity<?> duplicate()  {
		ResponseBean responseData = new ResponseBean();
		responseData.setStatusCode(ResponseCode.CONFLICT);
		responseData.setStatusMessage(ResponseMessage.CONFLICT);
		return ResponseEntity.status(ResponseCode.SUCCESS).body(responseData);
	}
	
	public static ResponseEntity<?> noData()  {
		ResponseBean responseData = new ResponseBean();
		responseData.setStatusCode(ResponseCode.NO_DATA);
		responseData.setStatusMessage(ResponseMessage.NO_DATA);
		return ResponseEntity.status(ResponseCode.SUCCESS).body(responseData);
	}

}
