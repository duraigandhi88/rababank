package com.rabobank.util;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.rabobank.beans.Account;
import com.rabobank.beans.Transaction;
/**
 * 
 * @author Gandhi
 *
 */

public class ConvertAccountDataCsv {
private static final Logger LOG=LogManager.getLogger(ConvertAccountDataToXml.class);
	
/**
 * 
 * @param writer
 * @param account
 */
public static void writeObjectToCSV(PrintWriter writer,Account account) {
    try (
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
                      .withHeader("TransactionDate", "AccountNumber", "Reference","Description","StartBalance","TransactionAmount","TransactionType","EndBalance"));
    ) {
    	if(account!=null) {
      for (Transaction transaction : account.getLiTransactions()) {
        List<String> data = Arrays.asList(
        		transaction.getTransactionDate().toString(),
            account.getAccountNumber(),
            Float.toString(transaction.getReference()),
            transaction.getDescription(),
            Float.toString(transaction.getStartBalance()),
            Float.toString(transaction.getTransactionAmount()),
            transaction.getTransactionType(),
            Float.toString(transaction.getEndBalance())
          );
        
        csvPrinter.printRecord(data);
      }
    	}
      
      csvPrinter.flush();
    } catch (Exception e) {
     LOG.error(e.getMessage());
    }
  }

}
