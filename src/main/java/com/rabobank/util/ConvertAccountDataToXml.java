package com.rabobank.util;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.rabobank.beans.Account;
import com.rabobank.beans.Transaction;
import com.rabobank.beans.TransactionAccount;
import com.rabobank.beans.TransactionAccounts;


public class ConvertAccountDataToXml {
	private static final Logger LOG=LogManager.getLogger(ConvertAccountDataToXml.class);
	
	public static String jaxbObjectToXML(Account account)
    {
		String xmlData="";
        try
		{        	
			// Create JAXB Context
			JAXBContext jaxbContext = JAXBContext.newInstance(TransactionAccounts.class);

			// Create Marshaller
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// Required formatting??
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			// Print XML String to Console
			StringWriter sw = new StringWriter();

			// Write XML to StringWriter
			jaxbMarshaller.marshal(changeToXml(account), sw);
			
			xmlData = sw.toString();

		} catch (JAXBException e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
        }
        
        return xmlData;
    }
	
	private static TransactionAccounts changeToXml(Account account){
		TransactionAccounts transactionAccounts=new TransactionAccounts();
		List<TransactionAccount> list=new ArrayList<TransactionAccount>();
		try {
			if(account!=null) {
		for(Transaction transaction:account.getLiTransactions()) {
			TransactionAccount transactionAccount=new TransactionAccount();
			transactionAccount.setTransactionDate(transaction.getTransactionDate());
			transactionAccount.setReference(transaction.getReference());
			transactionAccount.setAccountNumber(account.getAccountNumber());
			transactionAccount.setStartBalance(Float.toString(transaction.getStartBalance()));
			transactionAccount.setTransactionAmount(Float.toString(transaction.getTransactionAmount()));
			transactionAccount.setDescription(transaction.getDescription());
			transactionAccount.setEndBalance(Float.toString(transaction.getEndBalance()));
			transactionAccount.setTransactionType(transaction.getTransactionType());
			list.add(transactionAccount);
		}}
		}
		catch(Exception e) {
			LOG.error(e.getMessage());
		}
		transactionAccounts.setLiTransactionAccounts(list);
		
		return transactionAccounts;
		
	}


}
