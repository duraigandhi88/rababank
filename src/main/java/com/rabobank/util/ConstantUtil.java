package com.rabobank.util;
/**
 * 
 * @author Gandhi
 *
 */

public class ConstantUtil {
	
	public static final String WITHDRAW="withdraw";
	public static final String TRANSFER="transfer";
	public static final String SAVING="saving";

}
