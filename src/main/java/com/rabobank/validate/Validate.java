package com.rabobank.validate;

import org.springframework.http.ResponseEntity;

import com.rabobank.beans.Account;
import com.rabobank.beans.ResponseBean;
import com.rabobank.repository.AccountRepository;
import com.rabobank.util.ResponseData;
import com.rabobank.util.ResponseMessage;

public class Validate {
	
	private static final String userPattern="^[a-zA-Z]+[a-zA-Z0-9\\s.]{2,50}$";
	private static final String passwordPattern="^[a-zA-Z0-9!@#$%^&*()<>|{}?\\s.]{2,50}$";
	
	
	
	public static ResponseBean validateUserNameAndPassword(String userName,String password) {
		ResponseBean responseBean=null;
		if(userName==null || !userName.matches(userPattern)) {
			responseBean=new ResponseBean();
			responseBean.setStatusMessage(ResponseMessage.USERNAME_INVALID);
			return responseBean;
		}
		
		if(password==null || !password.matches(passwordPattern)) {
			responseBean=new ResponseBean();
			responseBean.setStatusMessage(ResponseMessage.PASSWORD_INVALID);
			return responseBean;
		}
		return responseBean;
	}
	
	public static String  isExistsUserNameAndEmailAndPhone(Account account,AccountRepository accountRepository) {
		String responseBean="";
		if(accountRepository.findByUserName(account.getUserName())!=null) {
			return responseBean=ResponseMessage.USERNAME_EXISTS;
		}
		else if(accountRepository.findByEmail(account.getEmail())!=null) {
			return responseBean=ResponseMessage.EMAIL_EXISTS;
		}
		else if(accountRepository.findByUserName(account.getUserName())!=null) {
			return responseBean=ResponseMessage.PHONE_EXISTS;
		}
		return responseBean;	
	}

}
