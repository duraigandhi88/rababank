package com.rabobank.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.rabobank.beans.Account;
import com.rabobank.beans.Transaction;
import com.rabobank.repository.AccountRepository;
import com.rabobank.repository.TransactionRepository;
import com.rabobank.util.AccountUtil;
import com.rabobank.util.ConstantUtil;
import com.rabobank.util.ResponseData;
import com.rabobank.util.ResponseMessage;
import com.rabobank.validate.Validate;

@Service
public class AccountService {

	private static final Logger LOG = LogManager.getLogger(AccountService.class);
	private static final SimpleDateFormat simple=new SimpleDateFormat("MM-dd-yyyy hh:ss:mm");

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private AccountUtil accountUtil;

	public ResponseEntity<?> saveAccount(Account account) {
		try {
			
			String existsUserNameAndEmailAndPhone=Validate.isExistsUserNameAndEmailAndPhone(account, accountRepository);
			if(existsUserNameAndEmailAndPhone.isEmpty()) {
			account.setAccountNumber(accountUtil.getRandomIBANAccountNumber());
			account.setCreateDate(new Date());
			LOG.info("Account created successfully");
			/**
			 * Initial Transaction
			 */
			Account newAccount = accountRepository.save(account);
			Transaction transaction = new Transaction();
			transaction.setDescription(ResponseMessage.INITIAL_AMOUNT);
			transaction.setStartBalance(account.getStartBalance());
			transaction.setEndBalance(account.getStartBalance());
			transaction.setReference(Long.parseLong(accountUtil.getRandomReferenceNumber().toString()));
			transaction.setTransactionDate(new Date());
			transaction.setTransactionType(ConstantUtil.SAVING);
			transaction.setTransactionAmount(account.getStartBalance());
			transaction.setAccount(newAccount);
			transactionRepository.save(transaction);
			return ResponseData.success(newAccount.getAccountNumber());
			}else {
				return ResponseData.validatitionFails(existsUserNameAndEmailAndPhone);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return ResponseData.internalError(e.getMessage());
		}
	}
	
	/**
	 * 
	 * @param accountNumber
	 * @return all transaction details
	 */

	public ResponseEntity<?> getAccountDetails(String accountNumber) {
		try {
			Account account = accountRepository.findByAccountNumber(accountNumber);
			if (account != null) {
				
				return ResponseData.successData(account);
			} else {
				return ResponseData.validatitionFails(ResponseMessage.ACCOUNT_NUMBER_INVALID);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return ResponseData.internalError(e.getMessage());
		}
	}

	public ResponseEntity<?> login(Account account) {
		try {
			LOG.info("Login initialize");
			return accountUtil.doLogin(account.getUserName(), account.getPassword());
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return ResponseData.internalError(e.getMessage());
		}
	}

	public ResponseEntity<?> transaction(String accountNumber, Transaction transaction) {
		try {
			Account account = accountRepository.findByAccountNumber(accountNumber);
			if (account != null) {

				transaction.setAccount(account);
				transaction.setReference(Long.parseLong(accountUtil.getRandomReferenceNumber().toString()));
				if (transaction.getTransactionType().equals(ConstantUtil.TRANSFER)) {
					LOG.info("fund transaction");
					transaction.setEndBalance(accountUtil.doCalEndBalanceWithTransfer(transaction, account));
				} else if (transaction.getTransactionType().equals(ConstantUtil.WITHDRAW)) {
					LOG.info("withdraw transaction");
					transaction.setEndBalance(accountUtil.doCalEndBalanceWithWithdraw(transaction, account));
				} else {
					LOG.info("Invalid transaction");
					return ResponseData.validatitionFails(ResponseMessage.TRANSACTION_INVALID);
				}
				LOG.info("transaction success");
				transaction.setTransactionDate(new Date());
				return ResponseData.success(transactionRepository.save(transaction).getReference().toString());
			} else {
				return ResponseData.validatitionFails(ResponseMessage.ACCOUNT_NUMBER_INVALID);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return ResponseData.internalError(e.getMessage());
		}
	}

}
