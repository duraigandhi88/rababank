package com.rabobank.beans;

import java.io.Serializable;



/**
 * 
 * @author gandhi.d
 *
 */

public class ResponseBean implements Serializable {

private static final long serialVersionUID = 12661L;
private Integer statusCode;
private String statusMessage;
private Object data;

public Integer getStatusCode() {
	return statusCode;
}
public void setStatusCode(Integer statusCode) {
	this.statusCode = statusCode;
}
public String getStatusMessage() {
	return statusMessage;
}
public void setStatusMessage(String statusMessage) {
	this.statusMessage = statusMessage;
}
public Object getData() {
	return data;
}
public void setData(Object data) {
	this.data = data;
}







}
