package com.rabobank.beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * @author Gandhi
 *
 */
@XmlRootElement(name = "transactionAccount")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionAccounts {

	private List<TransactionAccount> liTransactionAccounts=new ArrayList<>();

	public List<TransactionAccount> getLiTransactionAccounts() {
		return liTransactionAccounts;
	}

	public void setLiTransactionAccounts(List<TransactionAccount> liTransactionAccounts) {
		this.liTransactionAccounts = liTransactionAccounts;
	}
	
	
}
