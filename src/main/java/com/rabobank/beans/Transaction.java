package com.rabobank.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * 
 * @author Gandhi.D
 *
 */

@Entity
@Table(name="transaction")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Transaction implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 199659L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private Long reference;
	
	@NotNull
	@NotBlank(message="description is required")
	@Pattern(regexp="^[a-zA-Z]+[a-zA-Z0-9\\s.]{2,50}$",message="description should be valid one")
	private String description;
	
	@NotNull
	@Column(name="transaction_amount")
	private float transactionAmount;
	
	private Date transactionDate;
	private float endBalance;
	private float startBalance;
	
	@NotNull
	@NotBlank(message="transactionType is required")
	@Pattern(regexp="^(withdraw|transfer|saving)$",message="transactionType type has not a valid")
	@Column(name="transaction_type")
	private String transactionType;
	
	@ManyToOne
	@JoinColumn(name="account_id")
	private Account account;
	
	
	public Transaction() {
		
	}
	
	public Transaction(float transactionAmount,String transactionType,String description) {
		super();
		this.transactionAmount = transactionAmount;
		this.transactionType=transactionType;
		this.description=description;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Long getReference() {
		return reference;
	}
	public void setReference(Long reference) {
		this.reference = reference;
	}
	
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public float getEndBalance() {
		return endBalance;
	}
	public void setEndBalance(float endBalance) {
		this.endBalance = endBalance;
	}
	
	
	public Account getAccount() {
		return account;
	}


	public void setAccount(Account account) {
		this.account = account;
	}
	
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getTransactionType() {
		return transactionType;
	}


	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	
	


	public float getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(float transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	
	

	public float getStartBalance() {
		return startBalance;
	}

	public void setStartBalance(float startBalance) {
		this.startBalance = startBalance;
	}

	@Override
	public String toString() {
		return "Transaction [id:" + id + ", reference:" + reference + ", transactionAmount:" + transactionAmount + ", description="
				+ description + ", transactionType:" + transactionType
				+ ", transactionDate:" + transactionDate + ", endBalance:" + endBalance + "]";
	}
	
	
	
	
	
	
	

}
