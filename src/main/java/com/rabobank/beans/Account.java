package com.rabobank.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * 
 * @author Gandhi.D
 *
 */
@Entity
@Table(name="account")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Account implements Serializable {
	
	private static final long serialVersionUID = 435431L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
		
	@NonNull
	@NotBlank(message = "firstName is mandatory")
	@Pattern(regexp="^[a-zA-Z]+[a-zA-Z0-9\\s.]{2,50}$",message="First Name should be valid one")
    private String firstName;
	
	@Pattern(regexp="^[a-zA-Z]+[a-zA-Z0-9\\s.]{2,50}$",message="lastName should be valid one")
	private String lastName;
	
	@NonNull
	@NotBlank(message = "password is mandatory")
	@Pattern(regexp="^[a-zA-Z0-9!@#$%^&*()<>|?\\s.]{2,50}$",message="password should be valid one")
	private String password;
	
	@NonNull
	@NotBlank(message = "email is mandatory")
	@Email(message="email should be valid one")
	private String email;
	
	@NonNull
	@NotBlank(message = "phone is mandatory")
	@Pattern(regexp="^[0-9+\\s]{10,13}$",message="phone should be valid one")
	private String phone;
	
	@NonNull
	private Float startBalance;
	
	@Column(name="accountNumber")
	private String accountNumber;
	
	private Date createDate;
	
	private Date modifyDate;
	
	@NonNull
	@NotBlank(message = "Username is mandatory")
	@Pattern(regexp="^[a-zA-Z]+[a-zA-Z0-9\\s.]{2,50}$",message="Username should be valid one")
	private String userName;
	
	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
	private List<Transaction> liTransactions=new ArrayList<>();
	
	public Account() {
		
	}
	
	public Account(String password,String userName) {
		this.userName=userName;
		this.password = password;
	}
	
	
	public Account(String firstName, String lastName, String password,String userName,
			String email, String phone, Float startBalance) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName=userName;
		this.password = password;
		this.email = email;
		this.phone = phone;
		this.startBalance = startBalance;
	
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Float getStartBalance() {
		return startBalance;
	}
	public void setStartBalance(Float startBalance) {
		this.startBalance = startBalance;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public List<Transaction> getLiTransactions() {
		return liTransactions;
	}


	public void setLiTransactions(List<Transaction> liTransactions) {
		this.liTransactions = liTransactions;
	}


	@Override
	public String toString() {
		return "Account [id:" + id + ", firstName:" + firstName + ", lastName:" + lastName + ", password:" + password
				+ ", email:" + email + ", phone:" + phone + ", startBalance:" + startBalance + ", accountNumber="
				+ accountNumber + ", createDate:" + createDate + ", modifyDate:" + modifyDate + "]";
	}
	
	
	
	
	
	
	
	

}
