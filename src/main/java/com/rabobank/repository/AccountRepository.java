package com.rabobank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rabobank.beans.Account;

/**
 * 
 * @author Gandhi.D
 *
 */

public interface AccountRepository extends JpaRepository<Account, Integer> {
	
	public Account findByAccountNumber(String accountNumber);
	public Account findByUserNameAndPassword(String userName,String password);
	public Account findByUserName(String userName);
	public Account findByEmail(String email);
	public Account findByPhone(String phone);
}
