package com.rabobank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rabobank.beans.Transaction;

/**
 * 
 * @author Gandhi.D
 *
 */

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

}
