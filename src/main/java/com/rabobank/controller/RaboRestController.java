package com.rabobank.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rabobank.beans.Account;
import com.rabobank.beans.Transaction;
import com.rabobank.repository.AccountRepository;
import com.rabobank.service.AccountService;
import com.rabobank.util.ConvertAccountDataCsv;
import com.rabobank.util.ConvertAccountDataToXml;
/**
 * 
 * @author Gandhi
 *
 */
@RestController
@CrossOrigin("*")
public class RaboRestController {

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private AccountRepository accountRepository;

	@PostMapping("login")
	public ResponseEntity<?> login(@RequestBody Account account) {
		return accountService.login(account);
	}

	@PostMapping("createAccount")
	public ResponseEntity<?> createAccount(@Valid @RequestBody Account account) {
		return accountService.saveAccount(account);
	}

	@PostMapping("transaction/{accountNumber}")
	public ResponseEntity<?> saveTransaction(@PathVariable(value = "accountNumber") String accountNumber,
			@Valid @RequestBody Transaction transaction) {
		return accountService.transaction(accountNumber, transaction);
	}
	
	@GetMapping("transaction/{accountNumber}")
	public ResponseEntity<?> getTransactionDetails(@PathVariable(value = "accountNumber") String accountNumber) {
		return accountService.getAccountDetails(accountNumber);
	}
	
	  @GetMapping("/download/{accountNumber}/transaction.csv")
	  public void downloadCSV(HttpServletResponse response,@PathVariable(value = "accountNumber") String accountNumber) throws IOException{
	    response.setContentType("text/csv"); 
	    response.setHeader("Content-Disposition", "attachment; file=transaction.csv");
	    ConvertAccountDataCsv.writeObjectToCSV(response.getWriter(), accountRepository.findByAccountNumber(accountNumber));
	  }
	 
	  @GetMapping("/download/{accountNumber}/transaction.xml")
	public ResponseEntity<?> downloadFile(@PathVariable(value = "accountNumber") String accountNumber) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentDispositionFormData("transaction.xml", "transaction.xml");
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		return new ResponseEntity(ConvertAccountDataToXml.jaxbObjectToXML(accountRepository.findByAccountNumber(accountNumber)), headers,HttpStatus.OK);
	}
}
