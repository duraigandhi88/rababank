package com.rabobank;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.rabobank.beans.Account;


@RunWith(SpringRunner.class)
@SpringBootTest
public class RabobankApplicationTests {
	
	/**
	 * 
	 * Login test with wrong data
	 */
	@Test
	public void testForLogin() throws URISyntaxException
	{
	    RestTemplate restTemplate = new RestTemplate();
	    final String baseUrl = "http://localhost:8088/login";
	    Account account = new Account("test","test@123");
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	 
	    HttpEntity<Account> request = new HttpEntity<>(account, headers);
	     
	    try
	    {
	    	ResponseEntity<String> response=restTemplate.postForEntity(baseUrl, request, String.class);
	    	assertTrue(response.getBody().contains("400"));
	    	assertEquals(200, response.getStatusCodeValue());
	        //Assert.fail();
	    }
	    catch(HttpClientErrorException ex)
	    {
	        //Verify bad request and missing header
	        Assert.assertEquals(400, ex.getRawStatusCode());
	       
	    }
	}
	
	/**
	 * 
	 * FirstName required validation
	 */
	
	@Test
	public void testForCreateAccontValidation() throws URISyntaxException
	{
	    RestTemplate restTemplate = new RestTemplate();
	    final String baseUrl = "http://localhost:8088/createAccount";
	    Account account = new Account("test","test@123");
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	 
	    HttpEntity<Account> request = new HttpEntity<>(account, headers);
	     
	    try
	    {
	    	ResponseEntity<String> response=restTemplate.postForEntity(baseUrl, request, String.class);
	    	assertTrue(response.getBody().contains("firstName is mandatory"));
	    	assertTrue(response.getBody().contains("400"));
	    	assertEquals(200, response.getStatusCodeValue());
	        //Assert.fail();
	    }
	    catch(HttpClientErrorException ex)
	    {
	        //Verify bad request and missing header
	        Assert.assertEquals(400, ex.getRawStatusCode());
	       
	    }
	}

}
